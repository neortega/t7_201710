package model.logic;

import java.util.ArrayList;
import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import model.data_structures.*;
import model.vo.*;

public class Manejador {
	/**
	 * 
	 */
	private RedBlackBST<Integer, RedBlackBST<Integer, VOInfoPelicula>> principal;
	
	
	public Manejador()
	{
		principal = new RedBlackBST<Integer, RedBlackBST<Integer, VOInfoPelicula>>();
	}
	
	public RedBlackBST<Integer, RedBlackBST<Integer,VOInfoPelicula>> peliculasEntreRango(int a�o1, int a�o2)
	{
		if(principal.isEmpty() || !principal.contains(a�o1) || !principal.contains(a�o2))
			return null;
		
		RedBlackBST<Integer, RedBlackBST<Integer,VOInfoPelicula>> arbolRespuesta = new RedBlackBST<Integer,RedBlackBST<Integer,VOInfoPelicula>>();
		
		Iterator<Integer> iterator = principal.keys();
		
		while(iterator.hasNext())
		{
			int actual = iterator.next();
			
			if(actual >= a�o1 && actual <= a�o2)
			{
				arbolRespuesta.put(actual, principal.get(actual));
			}
			
			actual = iterator.next();
		}
		return arbolRespuesta;
	}
	
	public void cargar()
	{
		JsonParser json_parser = new JsonParser();
		
		try {
			JsonObject objPelicula = (JsonObject) json_parser.parse("./data/links_json.json");
			JsonArray arreglo = objPelicula.getAsJsonArray();
			for (int i = 0; i < arreglo.size(); i++) {
				JsonObject actual = (JsonObject)arreglo.get(i);
				int movieId = actual.get("movieId").getAsInt();
				int a�o = actual.get("Year").getAsInt();
				VOInfoPelicula nuevo = new VOInfoPelicula(actual.get("Title").toString(), actual.get("Director").toString(),
						actual.get("Actors").toString(),a�o, actual.get("movieId").getAsInt(), 
						actual.get("imdbRating").getAsDouble());
				if(principal.contains(a�o))
				{
					principal.get(a�o).put(movieId, nuevo);
				}
				else
				{
					RedBlackBST<Integer, VOInfoPelicula> nuevoArbol = new RedBlackBST<>();
					nuevoArbol.put(movieId, nuevo);
					principal.put(a�o, nuevoArbol);
				}	
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	public ListaEncadenada<Integer, RedBlackBST<Integer,VOInfoPelicula>> darA�osConMasPeliculas()
	{
		ListaEncadenada<Integer, RedBlackBST<Integer,VOInfoPelicula>> respuesta = new ListaEncadenada<Integer, RedBlackBST<Integer,VOInfoPelicula>>();
		
		Integer[] keysArray = new Integer[principal.size()];
		
		Iterator<Integer> iterator = principal.keys();
		
		for(int i = 0; iterator.hasNext() && i < principal.size(); i++)
		{
			Integer actual = iterator.next();
			keysArray[i] = actual;
		}
		
		for(int i = 0; i < keysArray.length; i++)
		{
			int max = i;
			for(int j=i+1; j < keysArray.length; j++)
			{
				if(principal.get(keysArray[j]).size()> principal.get(keysArray[max]).size())
					max = j;
				
				Integer temp = keysArray[i];
				keysArray[i] = keysArray[max];
				keysArray[max] = temp;
			}
		}
		
		for(int i = 0; i < keysArray.length; i++)
			respuesta.agregarElementoFinal(keysArray[i], principal.get(keysArray[i]));
		
		return respuesta;
	}
	
	public ArrayList<VOInfoPelicula> inOrden(Integer key)
	{
		RedBlackBST<Integer, VOInfoPelicula> aux = principal.get(key);
			
		return aux.inOrden();
	}
}
