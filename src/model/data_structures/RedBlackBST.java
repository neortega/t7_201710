package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class RedBlackBST<Key extends Comparable<Key>, Value> 
{
	public final static boolean RED = true;
	
	public final static boolean BLACK = false;

	private Node actual;
	
	public RedBlackBST()
	{
		actual = null;
	}
	
	public class Node
	{
		Key key;
		Value value;
		Node left, right;
		ListaEncadenada<Key, Value> nodeList; 
		int N;
		boolean color;
		
		Node(Key key, Value value, int N, boolean color)
		{
			this.key = key;
			this.value = value;
			this.N = N;
			this.color = color;
			this.nodeList = new ListaEncadenada<Key,Value>();
		}
	}
	
	public int size()
	{
		return size(actual);
	}
		
	private int size(Node x)
	{
		if(x == null) 
			return 0;
		
		else 
			return x.N;
	}
	
	public boolean isEmpty()
	{
		return size()==0;
	}
	
	public Value get(Key key)
	{
		return get(actual, key); 
	}
	
	private Value get(Node x, Key key)
	{
		if(x == null)
			return null;
		int cmp = key.compareTo(x.key);
		
		if(cmp < 0)
			return get(x.left, key);
		else if(cmp > 0)
			return get(x.right, key);
		else
			return x.value;
	}
	
	public boolean contains(Key key)
	{
		return get(key)!=null;
	}

	public void put(Key key, Value value)
	{
		actual = put(actual, key, value);
		actual.color = BLACK;
	}
	
	private Node put(Node x, Key key, Value value)
	{
		if(x == null)
			return new Node(key, value, 1, RED);
		
		int cmp = key.compareTo(x.key);
		
		if(cmp < 0)
			x.left = put(x.left, key, value);
			
		else if(cmp > 0)
			x.right = put(x.right, key, value);
		
		else
			x.value = value;
			
		if(isRed(x.right) && !isRed(x.left))
			x = rotateLeft(x);
		if(isRed(x.left) && isRed(x.left.left))
			x = rotateRight(x);
		if(isRed(x.left) && isRed(x.right))
			switchColors(x);
		
		x.N = size(x.left) + size(x.right) + 1;
		
		return x;
	}
	
	public int height()
	{
		return height(actual);
	}
	
	private int height(Node x)
	{
		if(x == null)
			return -1;
		
		return 1 + Math.max(height(x.left), height(x.right));
	}
	
	public Key min()
	{
		return min(actual);
	}
	
	private Key min(Node x)
	{
		if(x == null)
			return null;
		
		if(x.left == null)
			return x.key;
		
		else
			return min(x.left);
	}
	
	public Key max()
	{
		return max(actual);
	}
	
	private Key max(Node x)
	{
		if(x == null)
			return null;
		
		if(x.right == null)
			return x.key;
		
		else
			return max(x.right);
	}
	
	private void switchColors(Node x)
	{
		if(x != actual)
			x.color = RED;
		
		x.left.color = BLACK;
		x.right.color = BLACK;
	}
	
	private Node rotateLeft(Node h)
	{
		Node x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = h.color;
		h.color = RED;
		x.N = h.N;
		h.N = 1 + size(h.left) + size(h.right);
		
		return x;
	}
	
	private Node rotateRight(Node h)
	{
		Node x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = h.color;
		h.color = RED;
		x.N = h.N;
		h.N = 1 + size(h.left) + size(h.right);
		return x;
	}
	
	private boolean isRed(Node x)
	{
		if(x == null)
			return false;
		
		return x.color == RED;
	}
	
	public int rank(Key key)
	{
		return rank(key,actual);
	}
	
	private int rank(Key key, Node x)
	{
		if(key == null || x == null)
			return 0;
		int cmp = key.compareTo(x.key);
		if(cmp<0)
			return rank(key,x.left);
		
		else if(cmp>0)
			return 1 + size(x.left) + rank(key, x.right);
		
		else
			return size(x.left);
	}
	
	public boolean check()
	{
		return isBST(actual,null,null) && isSizeConsistent(actual) && is23(actual) && isBalanced();
	}
		
	private boolean isBST(Node x, Key min, Key max)
	{
		if(x==null)
			return true;
		if(min!=null && x.key.compareTo(min)<=0)
			return false;
		if(max!=null && x.key.compareTo(max)>=0)
			return false;
		return isBST(x.left, min, x.key) && isBST(x.right, x.key, max);
	}
	
	private boolean isSizeConsistent(Node x)
	{
		if(x==null)
			return true;
		if(x.N != (size(x.left) + size(x.right) + 1))
			return false;
		return isSizeConsistent(x.left) && isSizeConsistent(x.right);
	}
	
	private boolean is23(Node x)
	{
		if(x==null)
			return true;
		if(isRed(x.right))
			return false;
		if(x!=actual && isRed(x) && isRed(x.left))
			return false;
		return is23(x.left) && is23(x.right);
	}
	
	private boolean isBalanced()
	{
		int black = 0;
		Node x = actual;
		while(x!=null)
		{
			if(!isRed(x))
				black++;
			
			x = x.left;
		}
		return isBalanced(actual,black);
	}
	
	private boolean isBalanced(Node x, int black)
	{
		if(x==null)
			return black == 0;
		if(!isRed(x))
			black --;
		return isBalanced(x.left, black) && isBalanced(x.right, black);
	}
	
	public Iterator<Key> keys()
	{
		ListaEncadenada<Key,Key> listKeys = new ListaEncadenada<Key,Key>();
		getListKeys(actual, listKeys, min(), max());
		
		return listKeys.iterator();
	}
	
	private void getListKeys(Node x, ListaEncadenada<Key,Key> list, Key lo, Key hi)
	{
		list = new ListaEncadenada<Key, Key>();
		
		if(x == null)
			return;
		
		int cmplo = lo.compareTo(x.key);
		int cmphi = hi.compareTo(x.key);
		
		if(cmplo < 0)
			getListKeys(x.left, list, lo, hi);
		if(cmplo <= 0 && cmphi >= 0)
			list.agregarElementoFinal(x.key, x.key);
		if(cmplo > 0)
			getListKeys(x.right, list, lo, hi);
	}
	
	public ArrayList<Value> inOrden()
	{
		return inOrden(actual);
	}
	
	private ArrayList<Value> inOrden(Node x)
	{
		ArrayList<Value> inorden = new ArrayList<Value>();
		
		if(x == null)
			return null;
		
		if(x.left != null)
			inOrden(x.left);
		
		inorden.add(x.value);
		
		if(x.right != null)
			inOrden(x.right);
		
		return inorden;
	}
}
