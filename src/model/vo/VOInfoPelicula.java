package model.vo;

public class VOInfoPelicula {
	 
	private String titulo, director, actores;
	private int anio, movieID;
	private double ratingIMDB;
	
	public String getTitulo()
	{
		return titulo;
	}
	
	public VOInfoPelicula(String titulo, String director, String actores,
			int anio, int movieID, double ratingIMDB) {
		this.titulo = titulo;
		this.director = director;
		this.actores = actores;
		this.anio = anio;
		this.movieID = movieID;
		this.ratingIMDB = ratingIMDB;
	}

	public void setTitulo(String titulo)
	{
		this.titulo = titulo;
	}
	
	public String getDirector()
	{
		return director;
	}
	
	public void setDirector(String director)
	{
		this.director = director;
	}
	
	public String getActores()
	{
		return actores;
	}
	
	public void setActores(String actores)
	{
		this.actores = actores;
	}
	
	public int getAnio()
	{
		return anio;
	}
	
	public void setAnio(int anio)
	{
		this.anio = anio;
	}
	
	public int getMovieID()
	{
		return movieID;
	}
	
	public void setMovieID(int movieID)
	{
		this.movieID = movieID;
	}
	
	public double getRatingIMDB()
	{
		return ratingIMDB;
	}
	
	public void setRatingIMDB(double ratingIMDB)
	{
		this.ratingIMDB = ratingIMDB;
	}
}
